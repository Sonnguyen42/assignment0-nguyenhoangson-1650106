package net.codejava;

public class Knight {

	private int baseHP;
	private int wp;
	private int x=0;
	public Knight(int baseHP, int wp) {
		this.baseHP = baseHP;
		this.wp = wp;
	}
	public int getBaseHP() {
		return baseHP;
	}
	public int getWp(int i) {
		return wp;
	}
	public int getRealHP() {
		if(wp==1) {
			x = baseHP;
		}
		if(wp==0) {
			x = baseHP/10;
		}
		return x;
	}
}